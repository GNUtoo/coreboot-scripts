#!/bin/sh
set -e
set -x
usage()
{
	echo "$0 <path/to/image> [flashrom parameters]"
	exit 1
}

if [ $# -eq 0 ] ; then
	usage
fi

flash_chip="$(cat ../config/flash_chip.conf)"
programmer="$(cat ../config/programmer.conf)"

image="$1"
shift 1

flashrom  \
	--verbose \
	${flash_chip} \
	-w ${image} -o ${image}.log \
	${programmer} \
	$@
