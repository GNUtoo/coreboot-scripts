#!/bin/sh
set -e
usage()
{
	echo "$0 <path/to/image> [extra flashrom parameters]"
	exit 1
}

if [ $# -eq 0 ] ; then
	usage
fi

flash_chip="$(cat ../config/flash_chip.conf)"
image="$1"
shift 1

flashrom  \
	-p internal:ich_spi_force=yes \
	--verbose \
	${flash_chip} \
	-w "${image}" -o ${image}.log \
	$@
