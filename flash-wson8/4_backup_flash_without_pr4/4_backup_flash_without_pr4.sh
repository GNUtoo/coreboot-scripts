#!/bin/sh
set -e

usage()
{
  echo "Usage: $0"
  exit 1
}

if [ $# -ne 0 ] ; then
  usage
fi

flash_chip="$(cat ../config/flash_chip.conf)"

flashrom \
	${flash_chip} \
	-p internal:laptop=force_I_want_a_brick,ich_spi_force=yes \
	-l layout.txt \
	-i before_pr4 -r before_pr4.bin -o before_pr4.log

flashrom \
	${flash_chip} \
	-p internal:laptop=force_I_want_a_brick,ich_spi_force=yes \
	-l layout.txt \
	-i after_pr4 -r after_pr4.bin -o after_pr4.log
