#!/bin/sh
set -x
set -e

add_region()
{
    outfile="$1"
    infile="$2"
    ifd_rom="$3"
    partition="$4"

    partition_filename=""

    case ${partition} in
	"Descriptor")
	    partition_filename="flashregion_0_flashdescriptor.bin"
	    ;;
	"BIOS")
	    partition_filename="flashregion_1_bios.bin"
	    ;;
	"ME")
	    partition_filename="flashregion_2_intel_me.bin"
	    ;;
	"GbE")
	    partition_filename="flashregion_3_gbe.bin"
	    ;;
	"Platform")
	    partition_filename="flashregion_4_platform_data.bin"
	    ;;
	*)
	    echo "$0: add_region: Unknown partition '${partition}'"
	    exit 1
    esac

    ifdtool -x ${ifd_rom}
    dd conv=notrunc if=flashregion_0_flashdescriptor.bin of=${infile}
    ifdtool -x ${infile}
    ifdtool -i ${partition}:${partition_filename} ${outfile}
    mv -f ${outfile}.new ${outfile}
}

usage()
{
    echo "Usage: $0 <final_rom> <ifd_rom> <bios_rom> <me_rom> <gbe_rom> <platform_data_rom>"
    exit 1
}

if [ $# -ne 6 ] ; then
    usage
fi

final_rom="$(realpath $1)"
ifd_rom="$2"
bios_rom="$3"
me_rom="$4"
gbe_rom="$5"
platform_data_rom="$6"

# Work only on copies
tmpdir="$(mktemp -d)"

for file in ${ifd_rom} ${bios_rom} ${me_rom} ${gbe_rom} ${platform_data_rom} ; do
    cp ${file} ${tmpdir}/
done

for name in ifd_rom bios_rom me_rom gbe_rom platform_data_rom ; do
    eval ${name}=${tmpdir}/$(basename $(eval "echo \${${name}}"))
done

work_rom="${tmpdir}/final.rom"

cd ${tmpdir}

# Uncomment the lines below to add more debugging output
# echo "ifd_rom=${ifd_rom}"
# echo "bios_rom=${bios_rom}"
# echo "me_rom=${me_rom}"
# echo "gbe_rom=${gbe_rom}"
# echo "platform_data_rom=${platform_data_rom}"

cp -f ${ifd_rom} ${work_rom}
ifdtool -x ${ifd_rom}

add_region ${work_rom} ${bios_rom} ${ifd_rom} "BIOS"
add_region ${work_rom} ${me_rom} ${ifd_rom} "ME"
add_region ${work_rom} ${gbe_rom} ${ifd_rom} "GbE"
add_region ${work_rom} ${platform_data_rom} ${ifd_rom} "Platform"

cp "${work_rom}" "${final_rom}"
