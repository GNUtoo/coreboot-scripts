#!/bin/sh
set -e

usage()
{
  echo "Usage: $0 <path/to/boot/software.rom>"
  exit 1
}

if [ $# -ne 1 ] ; then
  usage
fi

flash_chip="$(cat ../config/flash_chip.conf)"
image="$1"

flashrom \
	${flash_chip} \
	-p internal:laptop=force_I_want_a_brick,ich_spi_force=yes \
	-w ${image} -o ${image}.log
