#!/bin/sh
set -e
usage()
{
	echo "$0 [extra flashrom parameters]"
	exit 1
}

if [ $# -ne 0 ] ; then
	usage
fi

full_image="../5_create_coreboot_image/flash.bin"
flash_chip="$(cat ../config/flash_chip.conf)"

ifdtool -f layout.txt ${full_image}

for region in fd gbe bios ; do
  flashrom \
	  -p internal:laptop=force_I_want_a_brick,ich_spi_force=yes \
	  ${flash_chip} \
	  -l layout.txt -N \
	  -w ${full_image} \
	  -i ${region} -o ${region}.log \
	  $@ || printf "\n${region}: Failed\n\n"
done

echo "$(basename $0): DONE"
