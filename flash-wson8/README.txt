== Description ==
This enables to flash GM45 thinkpads with WSON8 flash chip without soldering anything.
This applies to the:
- Thinkpad X200s
- Thinkpad X200 Tablet

Benefits:
- faster reflashing
- no soldering equipement needed
- no solder:
  - no regulatory issues (CE, RHOS)
  - no increase in health risk for the health of the user (Lead, resin)
  - no increase in health risk and the person flashing the laptop (no fumes)

It requires:
- Disassembling the laptop
- An external flasher and another computer to use the external flasher
- A modified SOIC-8 clip
- A recent flashrom (Flashrom 1.0 worked fine)

== Technical details on the flash read/write permissions ==
On the GM45 thinkpads supported by Coreboot and Libreboot, with the default
boot software, there are two types of protections against reading/writing
the flash chip internally (flashrom -p internal) that are set:
- The flash descriptor locks: this are bits set inside the flash descriptor which
  defines which processor (The main CPU, the Management Engine)
  have which permissions (read, write) on the flash regions.
  Here it sets:
  - the flash descriptor to read-only
  - the GBE (Ethernet chip) region to read-write
  - the BIOS region to read-write
  - the ME (Management Engine) region to not readable and not writable
  - the platform data region to read-write
- The Protected Range (PR) registers. Theses are set by the boot software
  (here the BIOS) and define again read and write permissions, here on
  arbitrary zones of the flash chip. here, there are 4 protected range
  registers, and only two of them are used:
  - PR0: it sets the end of the BIOS region (called the bootblock) to read-only.
  - PR4: it prevents reading and writing the platform region

If a restriction is set in one system but not in the other, the restriction
applies. For instance the platform data is set to read-write by the flash
descriptor, and set to be not readable and not writable by the PR4 register,
then flashrom will not be able to read it or to write it as the PR4 permissions
will prevent it to do so.

== How reflashing works ==
SOIC-8 and WSON-8 chips have the same pitch (space between the pins), so we
can take a SOIC-8 clip, deassemble it, and cut to expose the metallic
connections on the edge of the clip. This enables a human to connect the clip
to the WSON-8 pins by holding the two parts of the SOIC-8 clip.

Because a human has to hold the clip, reading or writing the full flash chip
this way would be unreliable as the connection would probably break before
the full reading or writing is done.

However the flash descriptor can be read internally by flashrom. And ifdtool
is able to change the flash descriptor permissions on a backup of it.
Its size is also very small (4k), so flashing the modified version is fast
enough to enable a human to hold the modified clip during the whole flashing
operation. The operation can also be retried multiple times as the flash
descriptor can be backed up.

Once the flash descriptor is reflashed externally, we can repartition
the flash chip, and shrink the BIOS region to 1M. This way the BIOS
region is:
- outside of the former read-only BIOS zone
- outside of the platform data region.

Coreboot is also able to build 1M images without a flash descriptor.

However we must not have flash zones that are not in a partition as
it would prevent flashrom from reading or writing to it. The reason
for that is unknown to me. Because of that the BIOS region was moved
inside the management engine region, and the management engine region
was moved at the end of the fash.
We had:
+----------------+-----------------+----------------+-------------+----+
|4k              | ~6M             | 8k             | 32k         | 2M |
|Flash descriptor|Management Engine|Gigabit Ethernet|Platform Data|BIOS|
+----------------+-----------------+----------------+-------------+----+
We now have:
+----------------+----------------+----+----+
|4k              | 8k             | 1M |~7M |
|Flash descriptor|Gigabit Ethernet|BIOS| ME |
+----------------+----------------+----+----+

Here we hope that the ME will not write in the new Gigabit Ethernet and
BIOS region. Using the MeAltDisable bit with the stock BIOS and unlocked
regions resulted in a non-booting computer, so we cannot use that.

Also some partition sizes are fixed:
- The flash descriptor is 4k
- The Gigabit Ethernet is 8k
- the platform data is 32k

So only the BIOS and ME partition sizes can be configured.

We then need to build Coreboot or Libreboot to fit inside the new
1M partition. To do that we build Coreboot or Libreboot by selecting 1M flash
size, and without a flash descriptor (IFD). We can then flash the new flash
descriptor again and Coreboot or Libreboot.

Once the BIOS is replaced with Coreboot or Libreboot, the computer has to
be rebooted. As Coreboot or Libreboot doesn't set the Protected Range
registers, all the flash is now read-write and a normal Coreboot or Libreboot
image can be reflashed.

== Note on backups ==
While the default boot software is proprietary, it might still be a good idea
to backup it if you need to do some work on coreboot or libreboot.
Running the original boot software, or part of it is sometimes required to
understand how software is supposed to talk to the hardware. It might also be
useful to develop better flashing procedures.

== Procedure with backups ==

1) Boot the computer and read the flash descriptor with flasrom -p internal
   Copy the flash descriptor in a new directory, as it will be needed later
   to create a backup image of the flash
2) Create a new modified flash descriptor with ifdtool to enable write on all
   the flash regions
3) Power off the computer and flash the new flash descriptor externally while
   holding the modified clip with your hands
4) Boot the computer and backup the flash zones before and after the region
   locked by PR4
5) Flash your boot software without touching to the boot block (read only
   BIOS region) and without touching to the platform region (which cannot be
   read because of PR4). You can modify:
   - the flash descriptor
   - the ME region
   - the GBE region
   - most of the BIOS region
6) Backup the flash chip again, to get the missing platform region
7) You now have access to the full flash so you can reflash coreboot or libreboot
