#!/bin/sh
set -e
usage()
{
	echo "$0 </path/to/image.rom> [extra flashrom parameters]"
	exit 1
}

if [ $# -eq 0 ] ; then
	usage
fi

coreboot_image="$1"
shift 1
full_image="flash.bin"
flash_chip="$(cat ../config/flash_chip.conf)"

# Create an image
# We only touch the flash descriptor and the bottom 1M
# of the BIOS region so the rest can be left blank
dd if=/dev/zero bs=512 count=16384 of=${full_image}

# Generate a new flash descriptor to set the BIOS region to 1M in the same location
bincfg ../config/ifd-x200.spec ifd-x200.set fd.bin
dd if=fd.bin of=${full_image} conv=notrunc

#And add coreboot
ifdtool --inject BIOS:${coreboot_image} ${full_image}
mv -f ${full_image}.new ${full_image}

# Generate a new GBE region and inject it
bincfg  ../config/gbe-ich9m.spec gbe-ich9m.set gbe.bin
ifdtool --inject GbE:gbe.bin ${full_image}
mv -f ${full_image}.new ${full_image}

echo "$(basename $0): DONE"
