#!/bin/sh
set -e

usage()
{
  echo "Usage: $0"
  exit 1
}

if [ $# -ne 0 ] ; then
  usage
fi

image="flash.bin"
flash_chip="$(cat ../config/flash_chip.conf)"
programmer="$(cat ../config/programmer.conf)"

dd if=/dev/zero bs=512 count=16384 of=${image}
cp ../2_create_modified_flash_descriptor/fd.bin ./
dd if=fd.bin of=${image} conv=notrunc

flashrom \
	${flash_chip} \
	${programmer} \
	-l layout.txt \
	-N -i fd -w ${image} -o fd.log
