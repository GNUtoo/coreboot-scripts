#!/bin/sh
set -e

file="../1_backup_flash_descriptor/fd.bin"
if [ ! -f ${file} ] ; then
  echo "${file} not found, maybe run 1_backup_flash_descriptor.sh"
  exit 1
fi
ifdtool -u "${file}"
mv ${file}.new ./fd.bin
