.PHONY: archive

../`basename $(PWD)`.tar.xz:
	git archive --format=tar HEAD > ../`basename $(PWD)`.tar
	xz -9 ../`basename $(PWD)`.tar
